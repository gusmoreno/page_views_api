Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get '/top_urls'      => 'page_views#top_urls'
  get '/top_referrers' => 'page_views#top_referrers'
end
