# Page Views' test

Ruby on Rails application, using [Sequel](http://sequel.jeremyevans.net) as its ORM, that allows its users to access two distinct reports in the realm of web stats using a REST API:

1. Number of page views per URL, grouped by day, for the past 5 days;

2. Top 5 referrers for the top 10 URLs grouped by day, for the past 5 days.

The dataset must be populated with 1.000.000 page view records.

## Getting Started

### Prerequisites

Ruby on Rails 5.2<br>
Ruby 2.5

### Installing

``` bundle install ```

### Seting up the database

If you have a postgres role at your local environment, change the settings in the file ```config/database.yml```. 

Create the databases for development and test environments

```
   rails db:create
   rails db:create RAILS_ENV=test
```

Then, run the following commands to migrate the databases:

```
   rails db:migrate
   rails db:migrate RAILS_ENV=test
```

### Populating the database

In order to easily populate the database, I created a Rake Task which will register 1.000.000 records by default, or the number specified by you in the task params. Simply run:

```
    rake dummyset:populate
    -- or --
    rake dummyset:populate[YOUR_TOTAL_RECORDS]
```

### Running the app

This app will run on port 3000 by running the following command:

``` rails s ```

## Tests

Tests were built with Rspec, and they include:

* Request Specs
* Model Specs
* Task Specs

### Running the tests

Simply run the following command:

``` rspec ```

### Test coverage

After running the tests, you can also see the test coverage by typing:

``` open coverage/index.html ```

## Consuming the API

Now that you have your dummy data you're ready to start tackling the two reports that the team has agreed to prototype.

### Report #1

End-user should be able to access a REST endpoint in order to retrieve the number of page views per URL, grouped by day, for the past 5 days.

Example request:

`[GET] /top_urls`

Example payload:

`{ '2017-01-01' : [ { 'url': 'http://apple.com', 'visits': 100 } ] }`

### Report #2

End-users should be able to retrieve the top 5 referrers for the top 10 URLs grouped by day, for the past 5 days, via a REST endpoint.

Example request:

`[GET] /top_referrers`

Example payload:
```
{
    '2017-01-01' : [
		{
			'url': 'http://apple.com',
			'visits': 100,			
			'referrers': [ { 'url': 'http://store.apple.com/us', 'visits': 10 } ]
	    }
	]
}
```