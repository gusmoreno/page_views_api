class PageViewsController < ApplicationController

  def top_urls
    page_views = PageView.top_urls

    render json: page_views, status: :ok
  end

  def top_referrers
    page_views = PageView.top_referrers(Date.today - 4.days, Date.today)

    render json: page_views, status: :ok
  end
end