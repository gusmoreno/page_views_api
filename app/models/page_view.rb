class PageView < Sequel::Model

  plugin :validation_helpers

  ##== VALIDATIONS ==========================
  def validate
    super
    validates_presence [:url, :created_at]
    validate_hash_size
  end

  def validate_hash_size
    unless self.values[:hash] && self.values[:hash].length == 32
      errors.add(:hash, 'must be 32 chars of length')
    end
  end

  ##== CLASS METHODS ==========================
  def self.set_hash(data)
    Digest::MD5.hexdigest({id: data[:id], url: data[:url], referrer: data[:referrer], created_at: data[:created_at]}.to_s)
  end

  def self.top_urls
    page_views = {}

    query_result = self.db["select DATE(created_at) as created_at, url, count(*) as visits from page_views
                          where DATE(created_at) >= ?
                          group by created_at, url
                          order by created_at, visits desc", (Date.today - 4.days)]

    query_result.map{ |q| q[:created_at] }.uniq.each do |row|
      page_views["#{row}"] = []
    end

    query_result.each do |row|
      page_views["#{row[:created_at]}"] << { 'url': row[:url], 'visits': row[:visits] }
    end
    page_views
  end

  def self.top_referrers(start_date, end_date)
    page_views = {}
    query_result = self.db["
      select top_urls_per_day.created_at, top_urls_per_day.url, top_urls_per_day.visits,
              top_referrers_per_url.referrer, top_referrers_per_url.referrer_amount
      from (SELECT date(generate_series(date(?), date(?), '1 day')) as visited_date) as generate_series
            inner join (
              select DATE(created_at) as created_at, url, count(*) as visits,
                     rank() OVER (partition by date(created_at) ORDER BY count(*) desc) as url_rank
              from page_views
                      where DATE(created_at) >= ?
                      group by date(created_at), url
                      order by visits desc
            ) as top_urls_per_day on top_urls_per_day.created_at = generate_series.visited_date and top_urls_per_day.url_rank <= 10
            inner join (
              select DATE(created_at) as created_at, url, referrer, count(*) as referrer_amount,
                     rank() OVER (partition by date(created_at), url ORDER BY count(*) desc) as referrer_rank
              from page_views
                      where DATE(created_at) >= ? AND referrer IS NOT NULL
                      group by date(created_at), url, referrer
                      order by referrer_amount desc
            ) as top_referrers_per_url on top_referrers_per_url.created_at = top_urls_per_day.created_at
               and top_referrers_per_url.url = top_urls_per_day.url
               and top_referrers_per_url.referrer_rank <= 5
       order by top_urls_per_day.created_at, top_urls_per_day.visits desc, top_urls_per_day.url, top_referrers_per_url.referrer_amount desc",
                           start_date, end_date, start_date, start_date]

    query_result.map{ |q| q[:created_at] }.uniq.each do |row|
      page_views["#{row}"] = []
    end

    date_aux, url_aux, visits_aux = nil
    referrers_aux = []
    query_result.each do |row|
      if date_aux == row[:created_at] && row[:url] == url_aux
        referrers_aux << { 'url': row[:referrer], 'visits': row[:referrer_amount] }
      else
        unless date_aux.nil?
          page_views["#{row[:created_at]}"] << {
              'url': url_aux,
              'visits': visits_aux,
              'referrers': referrers_aux
          }
        end
        date_aux = row[:created_at]
        url_aux = row[:url]
        visits_aux = row[:visits]
        referrers_aux = [{ 'url': row[:referrer], 'visits': row[:referrer_amount] }]
      end
    end
    page_views
  end
end
