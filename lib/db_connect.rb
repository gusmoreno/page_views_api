module DbConnect

  def connect_to_db
    if Rails.env == 'test'
      Sequel.connect(adapter: :postgres, user: '', password: '', host: 'localhost', port: 5432,
                     database: 'exercise2020_test', max_connections: 16, logger: Logger.new('log/db.log'))
    else
      Sequel.connect(adapter: :postgres, user: '', password: '', host: 'localhost', port: 5432,
                     database: 'exercise2020_development', max_connections: 16, logger: Logger.new('log/db.log'))
    end
  end

end