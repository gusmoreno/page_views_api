require 'faker'

namespace :dummyset do

  desc "Populate a dummy db with page_views"

  task :populate, [:total_inserts] => [:environment] do |task, args|

    TOTAL_INSERTS = args[:total_inserts].nil? ? 1000000 : args[:total_inserts].to_i

    def build_row(n, url, referrer)
      data = {
          id: n,
          url: url,
          referrer: referrer,
          created_at: DAYS.sample,
      }
      data[:hash] = PageView.set_hash(data)
      data
    end

    PageView.db[:page_views].delete

    REQUIRED_URLS = %w(http://apple.com https://apple.com https://www.apple.com
                       http://developer.apple.com http://en.wikipedia.org http://opensource.org)

    REQUIRED_REFERRERS = ['http://apple.com', 'https://apple.com', 'https://www.apple.com', 'http://developer.apple.com', nil]

    ALL_URLS = 50.times.map { Faker::Internet.url }

    DAYS = (DateTime.now-10.days..DateTime.now).to_a

    rows = []

    1.step(REQUIRED_URLS.count, 1) do |n|
      url = REQUIRED_URLS.sample
      rows << build_row(n, url, (REQUIRED_REFERRERS + ALL_URLS).sample)
      REQUIRED_URLS.delete(url)
      ALL_URLS.push(url)
    end

    (rows.count + 1).step(REQUIRED_REFERRERS.count, 1) do |n|
      referrer = REQUIRED_REFERRERS.sample
      rows << build_row(n, ALL_URLS.sample, referrer)
      REQUIRED_REFERRERS.delete(referrer)
      ALL_URLS.push(referrer)
    end

    (rows.count + 1).step(TOTAL_INSERTS, 1)  do |n|
      rows << build_row(n, ALL_URLS.sample, ALL_URLS.sample)
    end

    PageView.db.transaction do
      PageView.db[:page_views].multi_insert(rows)
    end

  end

end
