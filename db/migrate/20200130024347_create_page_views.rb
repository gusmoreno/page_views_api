Sequel.migration do
  change do

    create_table :page_views do
      primary_key :id
      String      :url,         text: true, null: false
      String      :referrer,    text: true
      DateTime    :created_at,  null: false
      String      :hash,        fixed: true, size: 32, null: false
    end
    add_index :page_views, :url
    add_index :page_views, :referrer
    add_index :page_views, :created_at
  end
end