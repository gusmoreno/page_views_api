require "rails_helper"
Rails.application.load_tasks

RSpec.describe 'dummyset:populate.rake', :type => :task do

  context 'when passing 50 as task argument' do

    let(:required_urls) { %w(http://apple.com https://apple.com https://www.apple.com
                             http://developer.apple.com http://en.wikipedia.org http://opensource.org) }

    let(:required_referrers) { ['phttp://apple.com', 'https://apple.com',
                                'https://www.apple.com', 'http://developer.apple.com', nil] }

    before { Rake::Task['dummyset:populate'].invoke(50) }

    it 'creates 50 page views' do
      expect(PageView.count).to be(50)
    end

    it 'ensures required urls were created' do
      expect(
        required_urls.all? { |url| PageView.where(url: url).exists }
      ).to be true
    end

    it 'ensures required referrers were created' do
      expect(
        required_referrers.all? { |ref| PageView.where(referrer: ref).exists }
      ).to be true
    end

  end

end