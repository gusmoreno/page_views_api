require "rails_helper"
Rails.application.load_tasks

RSpec.describe 'Page View', type: :request do

  before { Rake::Task['dummyset:populate'].invoke(50) }

  let(:last_five_days) { ((Date.today-4.days).strftime('%Y-%m-%d')..Date.today.strftime('%Y-%m-%d')).to_a }

  context 'GET /top_urls' do

    it 'returns a json with the top_urls from last 5 days and status ok' do
      get '/top_urls'

      json_response = JSON.parse(response.body)
      sample_page = json_response.values.sample.sample
      expect(
          json_response.keys.all?{ |k| last_five_days.include?(k) }
      ).to be true
      expect(sample_page).to have_key("url")
      expect(sample_page).to have_key("visits")
      expect(response).to have_http_status(:ok)
    end

  end

  context 'GET /top_referrers' do

    it 'returns a json with the top_referrers and status ok' do
      get '/top_referrers'

      json_response = JSON.parse(response.body)
      sample_page = json_response.values.sample.sample
      sample_page_referrer = sample_page["referrers"]
      expect(
          json_response.keys.all?{ |k| last_five_days.include?(k) }
      ).to be true
      expect(json_response.values.uniq.map(&:count).all?{|page| page <= 10}).to be true
      expect(json_response.values.sample.uniq.map(&:count).all?{|referrer| referrer <= 5}).to be true
      expect(sample_page).to have_key("url")
      expect(sample_page).to have_key("visits")
      expect(sample_page).to have_key("referrers")
      expect(sample_page_referrer.sample).to have_key("url")
      expect(sample_page_referrer.sample).to have_key("visits")
      expect(response).to have_http_status(:ok)
    end
  end

end