FactoryBot.define do
  factory :page_view do
    url        { Faker::Internet.url }
    referrer   { Faker::Internet.url }
    created_at { Date.today }
    hash       { nil }
  end
end