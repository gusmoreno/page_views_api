require 'rails_helper'

RSpec.describe PageView, :type => :model do

  let(:page_view) { build(:page_view) }

  it "has an invalid record" do
    expect(page_view).not_to be_valid
  end

  it "has a valid record" do
    page_view[:hash] = PageView.set_hash(page_view)
    expect(page_view).to be_valid
  end

  context 'migrations' do

    it{ is_expected.to have_column :url, type: String, allow_nil: false }
    it{ is_expected.to have_column :referrer, type: String, allow_nil: true }
    it{ is_expected.to have_column :created_at, type: "datetime", allow_nil: false }
    it{ is_expected.to have_column :hash, type: "character(32)", allow_nil: false }

  end

  context 'validations' do

    it { is_expected.to validate_presence :url }
    it { is_expected.to validate_presence :created_at }

  end

  context 'class methods' do

    it 'is expected to set a hash' do
      expect(page_view[:hash]).to be_nil
      page_view[:hash] = PageView.set_hash(page_view)
      expect(page_view[:hash]).not_to be_nil
    end

  end
end